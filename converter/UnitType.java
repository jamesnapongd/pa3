package converter;

/**
 * enum type of any unit.
 * @author Napong Dungduangsasitorn.
 *
 */
public enum UnitType {

	AREA{
		public Unit[] getUnit(){
			return Area.values();
		}
	},
	
	LENGTH{
		public Unit[] getUnit(){
			return Length.values();
		}
	},
	
	WEIGHT{
		public Unit[] getUnit(){
			return Weight.values();
		}
	},
	
	VOLUME{
		public Unit[] getUnit(){
			return Volume.values();
		}
	};

	public Unit[] getUnit(){
		return null;
	}

}




