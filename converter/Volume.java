package converter;

public enum Volume implements Unit{
	LITER("Liter",1.0),
	CUBIC_KILOMETER("Cube kilometer",1e-12),
	PICOLITER("Picoliter",1e+12),
	BARREL("Barrel",0.00611),
	GALLON("Gallon",0.219969),
	GWIAN("Gwian",2000);
	
	

	public final String name;
	public final double value;

	/**
	 * Constructor of Volume class.
	 * @param name is name of unit.
	 * @param value is value of that unit in volume unit.
	 */
	Volume(String name, double value){
		this.name = name;
		this.value = value;
	}

	/**
	 * convertTo method is use to convert unit to another unit.
	 * @param amount number of volume to convert to another unit.
	 * @param unit is unit that you want to change to.
	 */
	@Override
	public double convertTo(double amount, Unit unit) {

		double convert =  amount * this.getValue() / unit.getValue();
		return convert;
	}

	/**
	 * get value of volume.
	 */
	@Override
	public double getValue() {
		return this.value;
	}

	/**
	 * get name of volume unit
	 */
	public String toString(){
		return this.name;
	}

}
