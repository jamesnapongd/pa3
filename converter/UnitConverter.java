package converter;


public class UnitConverter {

	/**
	 * convert any unit to any unit.
	 * @param amount value that want to convert.
	 * @param fromUnit unit that want to convert to another unit.
	 * @param toUnit unit that want to convert.
	 * @return convert is value that convert from fromUnit to toUnit.
	 */
	public double convert(double amount, Unit fromUnit, Unit toUnit){
		return fromUnit.convertTo(amount, toUnit);
	}

	/**
	 * get unit from length class.
	 * @return units is array of unit from length class.
	 */
	public static Unit[] getUnits(UnitType unitType){

		return 	unitType.getUnit();
	}
	
	/**
	 * get array of type unit.
	 * @return array of type unit.
	 */
	public UnitType[] getUnitType(){
		return UnitType.values();
	}
	
	

}
