package converter;

import javax.swing.*;

import org.omg.Messaging.SyncScopeHelper;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * converter GUI 
 * @author Napong Dungduangsasitorn
 *
 */
public class ConverterUI extends JFrame 
{

	private UnitConverter uc;
	private JComboBox boxConvertFrom = new JComboBox();
	private JComboBox boxConvertTo = new JComboBox();
	private JMenuItem Area, Length, Volume, Weight;
	private JTextField txtConvertFrom  = new JTextField(15);
	private JTextField txtConvertTo = new JTextField(15);


	/**
	 * Constructor convertorUI.
	 */
	public ConverterUI(UnitConverter uc) {
		this.uc = uc;
		initComponents();
	}

	/**
	 * initialize components in the window
	 */
	private void initComponents() {

		setDefaultCloseOperation(EXIT_ON_CLOSE);


		JButton convertButton = new JButton("Convert");
		JButton clearButton = new JButton("Clear");


		JFrame frame = new JFrame();
		JPanel pane = new JPanel();

		pane.setLayout(new FlowLayout());


		JMenuBar menuBar = new JMenuBar();
		menuBar.setBackground(Color.GRAY);
		JMenu menu = new JMenu("Unit Type");
		menu.add(new ConverterAction("Area"));
		menu.add(new ConverterAction("Length"));
		menu.add(new ConverterAction("Volume"));
		menu.add(new ConverterAction("Weight"));
		menu.addSeparator();
		menu.add(new Exit());
		menuBar.add(menu);

		JLabel label = new JLabel(" = ");

	
		txtConvertFrom.setText("");
		txtConvertTo.setText("");


		txtConvertFrom.addActionListener(
				new ActionListener(){
					public void actionPerformed(ActionEvent e){
						try{
							if(Double.parseDouble(txtConvertFrom.getText())<0){
								txtConvertFrom.setForeground(Color.RED);
								JOptionPane.showMessageDialog(null,"Cant Compute Negative Number"
										,"Error",JOptionPane.WARNING_MESSAGE);
								txtConvertFrom.setForeground(Color.BLACK);
							}
							else{
								txtConvertTo.setText(String.format("%.6f",uc.convert(Double.parseDouble
										(txtConvertFrom.getText()), (Unit)boxConvertFrom.getSelectedItem(), 
										(Unit)boxConvertTo.getSelectedItem())));
								
							}
						} catch (NumberFormatException ex) {
							txtConvertFrom.setForeground(Color.RED);
							JOptionPane.showMessageDialog(null,"Please Insert Number","Error"
									,JOptionPane.WARNING_MESSAGE);
							txtConvertFrom.setForeground(Color.BLACK);

						}

					}
				});

		convertButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				try{
					if(Double.parseDouble(txtConvertFrom.getText())<0){
						JOptionPane.showMessageDialog(null,"Can't Compute Negative Number","Error",JOptionPane.WARNING_MESSAGE);
					}
					else{
						txtConvertTo.setText(String.format("%.6f",uc.convert(Double.parseDouble(txtConvertFrom.getText()), 
								(Unit)boxConvertFrom.getSelectedItem(), (Unit)boxConvertTo.getSelectedItem())));
					}
				} catch (NumberFormatException ex) {
					JOptionPane.showMessageDialog(null,"Please Insert Number","Error",JOptionPane.WARNING_MESSAGE);
				}
			}
		});


		clearButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				txtConvertFrom.setText("");
				txtConvertTo.setText("");
			}
		});


		
		

		pane.add(txtConvertFrom);
		pane.add(boxConvertFrom);

		pane.add(label);
		pane.add(txtConvertTo);
		pane.add(boxConvertTo);

		pane.add(convertButton);
		pane.add(clearButton);
		frame.add(pane);
		frame.pack();
		frame.setVisible(true);
	}


	class ConverterAction extends AbstractAction {
		private UnitType unitType;

		public ConverterAction(String unitName){
			super(unitName);
		}


		@Override
		public void actionPerformed(ActionEvent e) {
			txtConvertFrom.setText("");
			txtConvertTo.setText("");
			UnitType type = UnitType.valueOf(((JMenuItem) e.getSource()).getText().toUpperCase());
			Unit[] units = UnitConverter.getUnits(type);
			boxConvertFrom.removeAllItems();
			boxConvertTo.removeAllItems();
			for(Unit unit : units){
				boxConvertFrom.addItem(unit);
				boxConvertTo.addItem(unit);
			}
		}

	}
	
	class Exit extends AbstractAction {

		
		public Exit() {
			super("Exit");
		}

		
		public void actionPerformed( ActionEvent e ) {
			System.exit(0);
		}
	}

}






